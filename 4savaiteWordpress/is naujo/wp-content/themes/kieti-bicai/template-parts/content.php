<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mokslas
 */

?>
<article class="Second" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header id="FirstPart" class="entry-header">
		
			<?php
			if ( is_singular() ) :
				?>
				<p>
				<?php 
				the_title();
				?>
				</p>
				<?php
			else :
				the_title( '<p><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></p>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php kieti_bicai_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
			

		<p>
			<?php echo get_post_meta( $id, 'story', true );?>
		</p>

		<img src="<?php echo get_template_directory_uri();?>/img/linered.png">

		<!-- <p> -->
			<div class="entry-content">
					<?php
						the_content( sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'kieti-bicai' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						) );

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kieti-bicai' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
		<!-- </p> -->
		<?php the_post_thumbnail() ?>
		<!-- <img src="<?php echo get_template_directory_uri();?>/img/photo1.png">
		<img src="<?php echo get_template_directory_uri();?>/img/photo2.png">
		<img src="<?php echo get_template_directory_uri();?>/img/photo3.png" style="padding-right: 0;"> -->
	</header><!-- .entry-header -->
</article>
