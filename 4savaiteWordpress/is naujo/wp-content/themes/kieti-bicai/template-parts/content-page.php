<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mokslas
 */

?>
<header class="entry-header">
		<?php the_title( '<h1 class="burundukas">', '</h1>' ); ?>
</header><!-- .entry-header -->
<article class="poPietu" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<img src="<?php echo get_template_directory_uri();?>/img/poPietu1.png" style="padding-right: 40px;">
<div class="trys-karalaites">
	<?php  the_post_thumbnail(); ?>	
</div>

<div style="text-align: left;" id="poPietu1p">
	<!-- <p id="poPietu1p"> -->
	<!-- 	"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <br> quis nostrud exercitation." -->
		
		<?php
			the_content();
		?>
	<!-- </p> -->
	
	<img src="<?php echo get_template_directory_uri();?>/img/poPietu3.png" style="display: inline-block;">
	<p id="poPietu2p" style="display: inline-block;">
		<?php echo get_the_author() ?>
	</p>
	
</div>
<img src="<?php echo get_template_directory_uri();?>/img/poPietu4.png" style="padding-left: 40px;">
</article>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'kieti-bicai' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
