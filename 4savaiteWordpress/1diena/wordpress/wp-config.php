<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'biesas_wordpresas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mgQ-#(&eA5e}KEY$^<_U/83T4~:#R:df%-y.K|iXi6Id$Iw9spO2/$V3H*=C5((M');
define('SECURE_AUTH_KEY',  'h_Z&V=P`:jo5J9,-BAHJb@j:(Our(pCEsd@y*^`CrMu`AN?F YmHG:0v{)JS$OsH');
define('LOGGED_IN_KEY',    'G>S3Z!Hlg|Z#de4%+XZ`t<U1NP#:K0,ZEChZ8&2@Vm-[4]/RGE}BuK~8{ff2i?:D');
define('NONCE_KEY',        'PnBQHb/SG3/qM{(kO|PZ+0|$;v6<Gaht)]+2|.`Kha05GV{+E*`Ewp2sJ]G7N%a-');
define('AUTH_SALT',        'n#8d=-(m-<ytLeS}f@P%VopV=y!#X>&bX4z#=bqWQHerTMo`o%CPPqN5a5B802rx');
define('SECURE_AUTH_SALT', 'XON+WQV.$s J]f~|bwYQ?8cK0=L@n(6(5F3d0oX$R*a&E/ZH{:S9fbst=L/gH fM');
define('LOGGED_IN_SALT',   '-Whd)>FQN/?4#tjmns>?{^.76*Hc:5+96%Ukt>Q#L;7&y>*ZF`h7K|TLW53>A~t$');
define('NONCE_SALT',       'FDCns@+e|nW-9:6mF4)>XLqT#-d{p~123?0r,f61?)yBgyl,LaZ.S!>7Nl{$[G8I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
