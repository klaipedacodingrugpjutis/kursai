<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mokslas
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<?php if (is_front_page()) : ?>
	<div id="First">
	<header id="masthead" class="site-header">
		<nav id="Navigation">
			
    			<ul>
					<li><a href="#">MoGo</a></li>
				</ul>
			
			
			<ul>
				<li><a href="#"><img src="<?php echo get_template_directory_uri();?>/img/glass.png"></a></li>
				<li><a href="#"><img src="<?php echo get_template_directory_uri();?>/img/cart.png"></a></li>
			</ul>
			
			<ul>
			<?php
			$menu_ID = 5;
			$nav_menu = wp_get_nav_menu_object( $menu_ID );
			wp_nav_menu( array(
			                 'container'       => '',
			                 'container_class' => '',
			                 'items_wrap'      => '<ul><li><a id="item-id">'.$nav_menu->name.'</a></li></ul>%3$s'
			             )
			);
			?>
			</ul>
			
				<div id="Banner">
					<p>Creative Template</p>
					<p>WELCOME TO MOGO</p>
					<img src="<?php echo get_template_directory_uri();?>/img/line.png" align="middle">
					<button type="button">LEARN MORE</button>
				</div>

				<div id = "End">

				<div class="EndPart">
					<img src="<?php echo get_template_directory_uri();?>/img/linebot1redd.png">
					<p><span style="font-weight: 700;">01</span> INTRO</p>
				</div>

				<div class="EndPart">
					<img src="<?php echo get_template_directory_uri();?>/img/linebot1.png">
					<p><span style="font-weight: 700;">02</span> WORK</p>
				</div>

				<div class="EndPart">
					<img src="<?php echo get_template_directory_uri();?>/img/linebot1.png">
					<p><span style="font-weight: 700;">03</span> ABOUT</p>
				</div>

				<div class="EndPart">
					<img src="<?php echo get_template_directory_uri();?>/img/linebot1.png">
					<p><span style="font-weight: 700;">04</span> CONTACTS</p>
				</div>
				</div>
			<!-- <a href="#">Contact</a>
			<a href="#">Blog</a>
			<a href="#">Work</a>
			<a href="#">Service</a>
			<a href="#">About</a> -->
		</nav>
	</header><!-- #masthead -->
	</div><!-- #first -->	
	<?php endif; ?>
	<?php if (!is_front_page()) : ?>
	<div id="second">
	<header id="masthead" class="site-header">
		<nav id="Navigation">
			
 			<ul>
				<li><a href="#">MoGo</a></li>
			</ul>
			
			
			<ul>
				<li><a href="#"><img src="<?php echo get_template_directory_uri();?>/img/glass.png"></a></li>
				<li><a href="#"><img src="<?php echo get_template_directory_uri();?>/img/cart.png"></a></li>
			</ul>
			
			<ul>
			<?php
			$menu_ID = 5;
			$nav_menu = wp_get_nav_menu_object( $menu_ID );
			wp_nav_menu( array(
			                 'container'       => '',
			                 'container_class' => '',
			                 'items_wrap'      => '<ul><li><a id="item-id">'.$nav_menu->name.'</a></li></ul>%3$s'
			             )
			);
			?>
			</ul>
		</nav>
	</header><!-- #masthead -->
	</div>
	<?php endif; ?>
	<div id="content" class="site-content">
