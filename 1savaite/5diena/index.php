<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>5diena photoshop</title>
	<link href="https://fonts.googleapis.com/css?family=Alegreya:400,700|Playfair+Display:700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<section class="one">
		<header>
			<nav>
			   <div>
					<img src="img/Gustoso.png" alt="gustoso">
				</div>              
				<ul>
					<li><a href="">Welcome</a></li>
					<li>~</li>
					<li><a href="">Menu</a></li>
					<li>~</li>
					<li><a href="">Reservations</a></li>
					<li>~</li>
					<li><a href="">News</a></li>
					<li>~</li>
					<li><a href="">Contact</a></li>
				</ul>
				<ul>
					<li><a href=""><img src="img/social-icons.png"></a></li>
				</ul>
			</nav>
		</header>
		<h1>Pastry with love</h1>
		<img src="img/dashed-line.png">
		<p>We’re bringing you fresh ingredients every</p>
		<p> day in ways you can’t resist</p>
		<button>Our menu</button>
		
	</section>
	<section class="two">
		<div class="kepalas">
			<img src="img/kaliosikas.png">	
		</div>
		<div class="outter">
			<div class="left">
				<h1>Art of cakes</h1>
				<h2>We create delicious memories</h2>
				<p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed.</p>
				<h3>Chef Cook</h3>
				<img src="img/benito.png">
				<img src="img/benito-ismintis.png">
			</div>
			<div class="right">
				<div>
					<img src="img/img-top-left.jpg" alt="top left image">
					<img src="img/img-top-right.jpg" alt="">
				</div>
				<div>
					<img src="img/img-bottom-left.jpg" alt="bottom left">
					<img src="img/img-bottom-right.jpg" alt="right image">
					
				</div>
				<img src="img/taste-so-good.png" alt="taste so good">
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<div class="clearfix"></div>
	<section class="three">
		<div class="other">
			<div class="lefty">
				
			</div>
			<div class="righty">
				
			</div>
		</div>
	</section>
</body>
</html>